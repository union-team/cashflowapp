package com.spbd.cashflow.adapter;

import java.util.ArrayList;

import com.spbd.cashflow.app.R;
import com.spbd.cashflow.calculate.Guideline;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GuidelinesAdapter extends ArrayAdapter<Guideline> {

	public GuidelinesAdapter(Context context, ArrayList<Guideline> guidelines) {
		super(context, 0, guidelines);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Guideline guideline = getItem(position);

		if (convertView == null)
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_guideline, parent, false);

		TextView tvType = (TextView) convertView.findViewById(R.id.tvType);
		TextView tvMin = (TextView) convertView.findViewById(R.id.tvMin);
		TextView tvMax = (TextView) convertView.findViewById(R.id.tvMax);

		tvType.setText(guideline.getBusinessType());
		tvMin.setText(String.valueOf(guideline.getMinNetCashflow()));
		tvMax.setText(String.valueOf(guideline.getMaxNetCashflow()));

		return convertView;
	}

}
