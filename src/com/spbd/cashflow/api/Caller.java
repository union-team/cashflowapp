package com.spbd.cashflow.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Caller {
	public static String convertInputStreamToString(InputStream inputStream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String line = "";

		StringBuilder out = new StringBuilder();

		while ((line = bufferedReader.readLine()) != null)
			out.append(line);

		bufferedReader.close();
		inputStream.close();
		return out.toString();
	}
}
