package com.spbd.cashflow.app;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.spbd.cashflow.app.model.Cashflow;
import com.spbd.cashflow.app.model.UserDetails;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class BaseActivity extends Activity {

	protected Cashflow cashflow;
	protected ArrayList<Cashflow> cashflows;
	final public static String TAG = "Cashflow_App";
	public CashflowApplication myApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		myApp = (CashflowApplication) getApplication();
		myApp.setCurrentActivity(this);
		cashflow = myApp.getCashflow();
		// deleteCashflows();
		loadCashflows();
		loadUser();
	}

	protected void loadCashflows() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		int size = prefs.getInt("size", 0);
		cashflows = new ArrayList<Cashflow>();
		for (int i = 0; i < size; i++) {
			String json = prefs.getString("Cashflow_" + i, "");
			Gson gson = new Gson();
			Cashflow cashflow = gson.fromJson(json, Cashflow.class);
			if (cashflow != null)
				cashflows.add(cashflow);
		}
	}

	protected void saveCashflows() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		Editor editor = prefs.edit();
		Gson gson = new Gson();
		for (int i = 0; i < cashflows.size(); i++) {
			String json = gson.toJson(cashflows.get(i));
			editor.putString("Cashflow_" + i, json);
			editor.putInt("size", i + 1);
		}
		editor.commit();
	}

	protected void deleteCashflows() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		Editor editor = prefs.edit();
		int size = prefs.getInt("size", 0);
		for (int i = 0; i < size; i++)
			editor.remove("Cashflow_" + i);
		prefs.edit().remove("size");
		editor.commit();
	}

	protected void loadUser() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		String json = prefs.getString("userdetails", "");
		Gson gson = new Gson();
		UserDetails userDetails = gson.fromJson(json, UserDetails.class);
		if (userDetails != null) {
			myApp.setUsername(userDetails.getUsername());
			myApp.setPassword(userDetails.getPassword());
		}
	}
}
