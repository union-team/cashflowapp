package com.spbd.cashflow.app;

import java.util.ArrayList;

import com.spbd.cashflow.app.model.Cashflow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CashflowAdapter extends ArrayAdapter<Cashflow> {
	public CashflowAdapter(Context context, ArrayList<Cashflow> cashflows) {
		super(context, 0, cashflows);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Cashflow cashflow = getItem(position);
		if (convertView == null)
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_cashflow, parent, false);
		TextView tvName = (TextView) convertView.findViewById(R.id.itemNameTextView);
		TextView tvAmount = (TextView) convertView.findViewById(R.id.itemLoanAmountTextView);
		ImageView ivCheckmark = (ImageView) convertView.findViewById(R.id.ivCheckmark);

		tvAmount.setText(cashflow.getLoanAmount() + "");
		tvName.setText(cashflow.getName());
		if (cashflow.isUploaded())
			ivCheckmark.setVisibility(View.VISIBLE);
		else
			ivCheckmark.setVisibility(View.INVISIBLE);

		// Return the completed view to render on screen
		return convertView;
	}
}