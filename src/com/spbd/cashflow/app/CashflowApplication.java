/*
 Copyright 2013 Microsoft Corp
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package com.spbd.cashflow.app;

import com.spbd.cashflow.app.model.Cashflow;

import android.app.Activity;
import android.app.Application;

public class CashflowApplication extends Application {
	private Cashflow cashflow;
	private Activity currentActivity;
	private String username;
	private String password;

	public CashflowApplication() {
	}

	public Cashflow getCashflow() {
		if (cashflow == null)
			cashflow = new Cashflow();
		return cashflow;
	}

	public void setCurrentActivity(Activity activity) {
		currentActivity = activity;
	}

	public Activity getCurrentActivity() {
		return currentActivity;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setCashflow(Cashflow cashflow) {
		this.cashflow = cashflow;
	}
}
