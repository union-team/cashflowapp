package com.spbd.cashflow.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CashflowBudget extends BaseActivity {

	EditText etBusInflow;
	EditText etBusOutflow;
	TextView tvBusNetflow;

	EditText etHouseInflow;
	EditText etHouseOutflow;
	TextView tvHouseNetflow;

	double busInflow;
	double busOutflow;
	double busNetflow;

	double houseInflow;
	double houseOutflow;
	double houseNetflow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cashflow_budget);

		etBusInflow = (EditText) findViewById(R.id.etBusInflow);
		etBusOutflow = (EditText) findViewById(R.id.etBusOutflow);
		tvBusNetflow = (TextView) findViewById(R.id.tvBusNetflow);

		etHouseInflow = (EditText) findViewById(R.id.etHouseInflow);
		etHouseOutflow = (EditText) findViewById(R.id.etHouseOutflow);
		tvHouseNetflow = (TextView) findViewById(R.id.tvHouseNetflow);

		setupListeners();
	}

	private void setupListeners() {
		TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				change();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};
		etBusInflow.addTextChangedListener(textWatcher);
		etBusOutflow.addTextChangedListener(textWatcher);
		etHouseInflow.addTextChangedListener(textWatcher);
		etHouseOutflow.addTextChangedListener(textWatcher);
	}

	public void next(View v) {
		// TODO validation before allowing to go to next screen
		if (fielsAreValid()) {
			addValues();
			Intent intent = new Intent(getApplicationContext(), CashflowReview.class);
			startActivity(intent);
		} else
			Toast.makeText(this, "Please enter numbers greater than 0.", Toast.LENGTH_SHORT).show();
	}

	public void change() {
		int GREEN = Color.rgb(23, 172, 53);
		int RED = Color.RED;
		try {
			busInflow = Double.parseDouble(etBusInflow.getText().toString().trim());
			busOutflow = Double.parseDouble(etBusOutflow.getText().toString().trim());
			busNetflow = busInflow - busOutflow;
			tvBusNetflow.setText(String.format("%.1f", busNetflow));
			tvBusNetflow.setTextColor(busNetflow < 0.0 ? RED : GREEN);
		} catch (Exception e) {
		}

		try {
			houseInflow = Double.parseDouble(etHouseInflow.getText().toString().trim());
			houseOutflow = Double.parseDouble(etHouseOutflow.getText().toString().trim());
			houseNetflow = houseInflow - houseOutflow;
			tvHouseNetflow.setText(String.format("%.1f", houseNetflow));
			tvHouseNetflow.setTextColor(houseNetflow < 0.0 ? RED : GREEN);
		} catch (Exception e) {
		}
	}

	private void addValues() {
		cashflow.setExpectedBusinessInAverage(busInflow);
		cashflow.setExpectedBusinessOutAverage(busOutflow);
		cashflow.setExpectedBusinessNetAverage(busInflow - busOutflow);

		cashflow.setExpectedHouseholdInAverage(houseInflow);
		cashflow.setExpectedHouseholdOutAverage(houseOutflow);
		cashflow.setExpectedHouseholdNetAverage(houseInflow - houseOutflow);
	}

	private boolean fielsAreValid() {
		try {
			busInflow = Double.parseDouble(etBusInflow.getText().toString().trim());
			busOutflow = Double.parseDouble(etBusOutflow.getText().toString().trim());

			houseInflow = Double.parseDouble(etHouseInflow.getText().toString().trim());
			houseOutflow = Double.parseDouble(etHouseOutflow.getText().toString().trim());

			boolean ok = true;

			double min = 0.0;
			double max = 2000000.5;

			ok &= busInflow > min && busInflow < max;
			ok &= busOutflow > 0.0 && busOutflow < max;

			ok &= houseInflow > 0.0 && houseOutflow < max;
			ok &= houseOutflow > 0.0 && houseOutflow < max;

			return ok;
		} catch (Exception e) {
			return false;
		}
	}
}
