package com.spbd.cashflow.app;

import java.util.ArrayList;

import com.spbd.cashflow.app.model.Diary;
import com.spbd.cashflow.app.model.DiaryAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CashflowDiary extends BaseActivity {

	private Spinner spinnerMonths;
	private Button btnSubmitMonths;
	private DiaryAdapter adapter;
	private ListView listView;
	private ArrayList<Diary> arrayOfDiaries;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cashflow_diary);
		TextView tv = (TextView) findViewById(R.id.diaryTextView);
		tv.setText("Number of diary entries:");
		
		//Spinner functionality
		spinnerMonths = (Spinner) findViewById(R.id.spinnerMonths);
		btnSubmitMonths = (Button) findViewById(R.id.btnSubmitMonths);
		btnSubmitMonths.setOnClickListener(new OnClickListener() {
		  @Override
		  public void onClick(View v) {
			//Refresh Listview
			refreshListView(spinnerMonths.getSelectedItemPosition()+1);
			//Toast
		    Toast.makeText(CashflowDiary.this,
			"Diary entries refreshed to " 
	                + String.valueOf(spinnerMonths.getSelectedItem()) 
	                + " weeks",
				Toast.LENGTH_SHORT).show();
		  }
		});
		
		// Construct the data source
	    arrayOfDiaries = new ArrayList<Diary>();
		// Create the adapter to convert the array to views
		adapter = new DiaryAdapter(this, arrayOfDiaries);
		// Attach the adapter to a ListView
		listView = (ListView) findViewById(R.id.listDiaries);
		listView.setAdapter(adapter);
		
	}

	public void refreshListView(int months){
		adapter.clear();
		// Add items to adapter
		for(int nr = 0; nr < months; nr++){
			Diary newDiary = new Diary();
			newDiary.setMonthNumber(nr+1);
			adapter.add(newDiary);
		}
	}

	
	public void next(View v) {
		//validation before allowing to go to next screen
		boolean validatedInput = false;
		for (int i = 0; i < arrayOfDiaries.size(); i++) {
			Diary diary = arrayOfDiaries.get(i);
			if(diary.getBusinessIn() >= 0 
					&& diary.getBusinessOut() >= 0
					&& diary.getHouseholdIn() >= 0
					&& diary.getHouseholdOut() >= 0)
				validatedInput = true;
		}
		
		if(!validatedInput) {
			//Toast
		    Toast.makeText(CashflowDiary.this,
			"Please review inputted data, no negative values allowed",
				Toast.LENGTH_SHORT).show();
		} else {
			//add diaries to cashflow object
			cashflow.setDiaries(arrayOfDiaries);
		
	    
			//diary calculations
			cashflow.setNumberOfDiaryMonths(arrayOfDiaries.size() + 1);
			double diaryBusinessInTotal = 0;
			double diaryBusinessOutTotal = 0;
			double diaryBusinessNetTotal = 0;
			double diaryHouseholdInTotal = 0;
			double diaryHouseholdOutTotal = 0;
			double diaryHouseholdNetTotal = 0;
			for(int i = 0; i < arrayOfDiaries.size(); i++){
				diaryBusinessInTotal = diaryBusinessInTotal + arrayOfDiaries.get(i).getBusinessIn();
				diaryBusinessOutTotal = diaryBusinessOutTotal + arrayOfDiaries.get(i).getBusinessOut();
				diaryBusinessNetTotal = diaryBusinessNetTotal + (arrayOfDiaries.get(i).getBusinessIn()-arrayOfDiaries.get(i).getBusinessOut());
				diaryHouseholdInTotal = diaryHouseholdInTotal + arrayOfDiaries.get(i).getHouseholdIn();
				diaryHouseholdOutTotal = diaryHouseholdOutTotal + arrayOfDiaries.get(i).getHouseholdOut();
				diaryHouseholdNetTotal = diaryHouseholdNetTotal + (arrayOfDiaries.get(i).getHouseholdIn()-arrayOfDiaries.get(i).getHouseholdOut());
			}
			cashflow.setDiaryBusinessInAverage(diaryBusinessInTotal/cashflow.getNumberOfDiaryMonths());
			cashflow.setDiaryBusinessOutAverage(diaryBusinessOutTotal/cashflow.getNumberOfDiaryMonths());
			cashflow.setDiaryBusinessNetAverage(diaryBusinessNetTotal/cashflow.getNumberOfDiaryMonths());
			cashflow.setDiaryHouseholdInAverage(diaryHouseholdInTotal/cashflow.getNumberOfDiaryMonths());
			cashflow.setDiaryHouseholdOutAverage(diaryHouseholdOutTotal/cashflow.getNumberOfDiaryMonths());
			cashflow.setDiaryHouseholdNetAverage(diaryHouseholdNetTotal/cashflow.getNumberOfDiaryMonths());
	    
			Intent intent = new Intent(getApplicationContext(), CashflowBudget.class);
			startActivity(intent);
		}
	}
}

