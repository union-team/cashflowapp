package com.spbd.cashflow.app;

import java.util.Date;
import java.util.UUID;

import com.spbd.cashflow.app.model.Cashflow;
import com.spbd.cashflow.calculate.CashflowAnalysis;
import com.spbd.cashflow.calculate.Criteria;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CashflowOutput extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cashflow_output);
		/*
		 * TextView tv = (TextView) findViewById(R.id.outputTextView);
		 * tv.setText("Output! Loan amount: " + cashflow.getLoanAmount());
		 */

		// Populate Output Fields
		populateFields();
	}

	public void accept(View v) {
		// TODO validation before allowing to go to next screen
		cashflow.setDateSubmitted(new Date());
		cashflow.setAccepted(true);
		String uid = UUID.randomUUID().toString();
		cashflow.setMobileId(uid);
		cashflows.add(cashflow);
		saveCashflows();
		cashflow = null;

		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void reject(View v) {
		// TODO validation before allowing to go to next screen
		cashflow.setDateSubmitted(new Date());
		cashflow.setAccepted(false);
		String uid = UUID.randomUUID().toString();
		cashflow.setMobileId(uid);
		cashflows.add(cashflow);
		saveCashflows();
		cashflow = null;

		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	private void populateFields() {
		CashflowAnalysis cashflowAnalysis = new CashflowAnalysis(cashflow);
		cashflowAnalysis.calculateOutput();
		cashflow = cashflowAnalysis.getCashflow();

		// cashflow analysis 1
		setText(R.id.output_value1, cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiary());
		setText(R.id.output_risk1, cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk());

		// cashflow analysis 2
		setText(R.id.output_value2, cashflow.getNetBusinessIncomeAsSalesRevenueBudget());
		setText(R.id.output_risk2, cashflow.getNetBusinessIncomeAsSalesRevenueBudgetRisk());

		// cashflow analysis 3

		setText(R.id.output_value3, cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusiness());
		setText(R.id.output_risk3, cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk());

		// cashflow analysis 4
		setText(R.id.output_value4, cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiary());
		setText(R.id.output_risk4, cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk());

		// cashflow analysis 5

		setText(R.id.output_value5, cashflow.getLoanRepaymentOfHouseholdIncomeBudget());
		setText(R.id.output_risk5, cashflow.getLoanRepaymentOfHouseholdIncomeBudgetRisk());

		// cashflow analysis 6
		setText(R.id.output_value6, cashflow.getDifferenceBetweenPresentAndExpectedHousehold());
		setText(R.id.output_risk6, cashflow.getDifferenceBetweenPresentAndExpectedHouseholdRisk());

		// cashflow analysis 7

		setText(R.id.output_value7, cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiary());
		setText(R.id.output_risk7, cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk());

		// cashflow analysis 8
		setText(R.id.output_value8, cashflow.getHouseholdExpensesOfHouseholdIncomeBudget());
		setText(R.id.output_risk8, cashflow.getHouseholdExpensesOfHouseholdIncomeBudgetRisk());

		// Cashflow SUMMARY
		setText(R.id.output_summary, cashflow.getSummary());
		// cashflow.setSummary = cashflowAnalysis.getCashflow().getSummary();

	}

	private void setText(int id, String value) {
		TextView tv = (TextView) findViewById(id);
		if (value.equals(Criteria.HIGHRISK))
			tv.setBackgroundColor(getResources().getColor(R.color.highrisk_color));
		if (value.equals(Criteria.MEDRISK))
			tv.setBackgroundColor(getResources().getColor(R.color.medrisk_color));
		if (value.equals(Criteria.LOWRISK))
			tv.setBackgroundColor(getResources().getColor(R.color.lowrisk_color));
		tv.setText(value);
	}

	private void setText(int id, double value) {
		TextView tv = (TextView) findViewById(id);
		tv.setText(Double.toString(value) + "%");
	}

}
