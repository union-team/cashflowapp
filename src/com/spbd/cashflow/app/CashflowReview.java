package com.spbd.cashflow.app;

import java.util.ArrayList;
import java.util.List;

import com.spbd.cashflow.adapter.GuidelinesAdapter;
import com.spbd.cashflow.calculate.Guideline;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CashflowReview extends BaseActivity {

	TextView tvLoanAmount;

	TextView tvDiaryBusInflowShow, tvDiaryBusOutflowShow, tvDiaryBusNetflowShow;
	TextView tvBudgetBusInflowShow, tvBudgetBusOutflowShow, tvBudgetBusNetflowShow;

	TextView tvDiaryHouseInflowShow, tvDiaryHouseOutflowShow, tvDiaryHouseNetflowShow;
	TextView tvBudgetHouseInflowShow, tvBudgetHouseOutflowShow, tvBudgetHouseNetflowShow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cashflow_review);

		// set up textviews
		tvLoanAmount = (TextView) findViewById(R.id.tvLoanAmountShow);

		tvDiaryBusInflowShow = (TextView) findViewById(R.id.tvDiaryBusInflowShow);
		tvDiaryBusOutflowShow = (TextView) findViewById(R.id.tvDiaryBusOutflowShow);
		tvDiaryBusNetflowShow = (TextView) findViewById(R.id.tvDiaryBusNetflowShow);

		tvBudgetBusInflowShow = (TextView) findViewById(R.id.tvBudgetBusInflowShow);
		tvBudgetBusOutflowShow = (TextView) findViewById(R.id.tvBudgetBusOutflowShow);
		tvBudgetBusNetflowShow = (TextView) findViewById(R.id.tvBudgetBusNetflowShow);

		tvDiaryHouseInflowShow = (TextView) findViewById(R.id.tvDiaryHouseInflowShow);
		tvDiaryHouseOutflowShow = (TextView) findViewById(R.id.tvDiaryHouseOutflowShow);
		tvDiaryHouseNetflowShow = (TextView) findViewById(R.id.tvDiaryHouseNetflowShow);

		tvBudgetHouseInflowShow = (TextView) findViewById(R.id.tvBudgetHouseInflowShow);
		tvBudgetHouseOutflowShow = (TextView) findViewById(R.id.tvBudgetHouseOutflowShow);
		tvBudgetHouseNetflowShow = (TextView) findViewById(R.id.tvBudgetHouseNetflowShow);

		// populate textviews
		tvLoanAmount.setText(String.valueOf(cashflow.getLoanAmount()));

		tvDiaryBusInflowShow.setText(String.valueOf(cashflow.getDiaryBusinessInAverage()));
		tvDiaryBusOutflowShow.setText(String.valueOf(cashflow.getDiaryBusinessOutAverage()));
		tvDiaryBusNetflowShow.setText(String.valueOf(cashflow.getDiaryBusinessNetAverage()));

		tvBudgetBusInflowShow.setText(String.valueOf(cashflow.getExpectedBusinessInAverage()));
		tvBudgetBusOutflowShow.setText(String.valueOf(cashflow.getExpectedBusinessOutAverage()));
		tvBudgetBusNetflowShow.setText(String.valueOf(cashflow.getExpectedBusinessNetAverage()));

		tvDiaryHouseInflowShow.setText(String.valueOf(cashflow.getDiaryHouseholdInAverage()));
		tvDiaryHouseOutflowShow.setText(String.valueOf(cashflow.getDiaryHouseholdOutAverage()));
		tvDiaryHouseNetflowShow.setText(String.valueOf(cashflow.getDiaryHouseholdNetAverage()));

		tvBudgetHouseInflowShow.setText(String.valueOf(cashflow.getExpectedHouseholdInAverage()));
		tvBudgetHouseOutflowShow.setText(String.valueOf(cashflow.getExpectedHouseholdOutAverage()));
		tvBudgetHouseNetflowShow.setText(String.valueOf(cashflow.getExpectedHouseholdNetAverage()));
	}

	public void next(View v) {
		// TODO validation before allowing to go to next screen
		Intent intent = new Intent(getApplicationContext(), CashflowOutput.class);
		startActivity(intent);
	}

	public void realisticNo(View v) {
		// When NO is clicked
		Intent intent = new Intent(getApplicationContext(), CashflowStart.class);
		startActivity(intent);
		finish();
	}

	public void showGuidelines(View v) {
		// When guidelines is clicked
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Guidelines");

		GuidelinesAdapter adapter = new GuidelinesAdapter(this, getGuidelines());
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
			}
		});

		builder.create().show();
	}

	public ArrayList<Guideline> getGuidelines() {
		ArrayList<Guideline> guidelines = new ArrayList<Guideline>();
		guidelines.add(new Guideline("BBQ", 0, 380));
		guidelines.add(new Guideline("Bakery", 40, 180));
		guidelines.add(new Guideline("Beauty/hairdresser", 0, 0));
		guidelines.add(new Guideline("Canteen", 30, 80));
		guidelines.add(new Guideline("Fish selling", 15, 130));
		guidelines.add(new Guideline("Florist", 77, 450));
		guidelines.add(new Guideline("Food service", 20, 125));
		guidelines.add(new Guideline("Freshwater Mussels (Kai)", 55, 185));
		guidelines.add(new Guideline("Frozen Food - Ice Cream", 20, 70));
		guidelines.add(new Guideline("Frozen Food - Meat", 30, 40));
		guidelines.add(new Guideline("Fuel Kerosene", 10, 10));
		guidelines.add(new Guideline("Handicrafts", 80, 330));
		guidelines.add(new Guideline("Kava", 0, 70));
		guidelines.add(new Guideline("Lawn Care (Brush cutter)", 10, 38));
		guidelines.add(new Guideline("Livestock - Poultry/Pigs", 0, 0));
		guidelines.add(new Guideline("MPAISA", 0, 165));
		guidelines.add(new Guideline("Screen Painting", 0, 0));
		guidelines.add(new Guideline("Sewing", 30, 130));
		guidelines.add(new Guideline("Market Vendor", 130, 900));
		guidelines.add(new Guideline("Weaving", 0, 0));
		return guidelines;
	}
}
