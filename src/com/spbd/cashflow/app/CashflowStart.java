package com.spbd.cashflow.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class CashflowStart extends BaseActivity {

	private EditText amountEditText;
	private EditText nameEditText;
	private Button dateButton;

	public static int year;
	public static int month;
	public static int day;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cashflow_start);
		amountEditText = (EditText) findViewById(R.id.startAmountEditText);
		nameEditText = (EditText) findViewById(R.id.startNameEditText);
		dateButton = (Button) findViewById(R.id.btnDate);
	}

	public void next(View v) throws ParseException {
		double amount = -1;
		try {
			amount = Double.parseDouble(amountEditText.getText().toString());
		} catch (Exception e) {
			Toast.makeText(this, "Please specify a valid amount", Toast.LENGTH_SHORT).show();
		}

		if (CashflowStart.year == 0 && CashflowStart.month == 0 && CashflowStart.day == 0)
			Toast.makeText(this, "Please enter date of birth", Toast.LENGTH_SHORT).show();

		else if (amount >= 0.0 && nameEditText.getText().length() > 0) {
			cashflow.setLoanAmount(amount);
			cashflow.setName(nameEditText.getText().toString().trim());

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String dateStr = CashflowStart.day + "-" + CashflowStart.month + "-" + CashflowStart.year;
			date = sdf.parse(dateStr);
			Log.d(BaseActivity.TAG, date.toString());
			cashflow.setDateOfBirth(date);

			Intent intent = null;
			if (amount > 1500.0)
				intent = new Intent(getApplicationContext(), CashflowDiary.class);
			else
				intent = new Intent(getApplicationContext(), CashflowBudget.class);
			startActivity(intent);
		} else
			Toast.makeText(this, "Please enter valid data", Toast.LENGTH_SHORT).show();
	}

	public void openDate(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}

	static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			CashflowStart.year = year;
			CashflowStart.month = month + 1;
			CashflowStart.day = day;
		}
	}
}
