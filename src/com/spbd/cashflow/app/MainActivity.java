package com.spbd.cashflow.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.spbd.cashflow.api.Endpoint;
import com.spbd.cashflow.app.model.Cashflow;
import com.spbd.cashflow.app.model.UserDetails;
import com.spbd.cashflow.calculate.Security;

public class MainActivity extends BaseActivity {

	public final int TIMEOUT = 5000;
	public final String ERROR = "ERROR: Bad connection";
	public String httpResponseStr;
	public boolean ok;
	private ProgressBar pb;
	CashflowAdapter adapter;
	Stack<Cashflow> tasksToDo;
	Cashflow currentCashflow;
	Gson gson;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		pb = (ProgressBar) findViewById(R.id.progressBar);
		pb.setVisibility(View.GONE);
		// Construct the data source
		ArrayList<Cashflow> arrayOfUsers = new ArrayList<Cashflow>();
		// Create the adapter to convert the array to views
		adapter = new CashflowAdapter(this, arrayOfUsers);
		// Attach the adapter to a ListView
		ListView listView = (ListView) findViewById(R.id.mainListView);
		listView.setAdapter(adapter);
		Log.d(BaseActivity.TAG, "Cashflows size: " + cashflows.size());
		adapter.addAll(cashflows);
	}

	public void startCashflow(View v) {
		Intent intent = new Intent(getApplicationContext(), CashflowStart.class);
		startActivity(intent);
	}

	public void refreshNow() {
		// TODO check carefully that everything got updated
		// only first one is getting updated
		if (ok) {
			currentCashflow.setUploaded(true);
			saveCashflows();
			adapter.notifyDataSetChanged();
			// Toast.makeText(this, "Updated successfully",
			// Toast.LENGTH_SHORT).show();
			if (tasksToDo.size() > 0) {
				currentCashflow = tasksToDo.pop();
				ok = false;
				// new MyAsyncTask().execute(gson.toJson(currentCashflow));
			} else {
				Toast.makeText(this, "Successfully updated", Toast.LENGTH_SHORT).show();
				pb.setVisibility(View.GONE);
			}

		} else {
			// Log.d(BaseActivity.TAG, httpResponseStr);
			String error = "";
			if (httpResponseStr.length() < 2 || httpResponseStr.length() > 100)
				error = ERROR;
			else
				error = httpResponseStr;
			Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
			pb.setVisibility(View.GONE);
		}
		// Log.d(BaseActivity.TAG, "HAPPENS here");
	}

	public void sync(View v) {
		Log.d(BaseActivity.TAG, "sync() START");
		loadUser();
		if (myApp.getUsername() != null && myApp.getPassword() != null)
			new RestAPIConnector().execute("url");
		else
			Toast.makeText(this, "Enter User Details to Synch", Toast.LENGTH_SHORT).show();
		/*
		 * if (pb.getVisibility() != View.VISIBLE) { loadUser();
		 * Log.d(BaseActivity.TAG, "sync() START"); ok = false; httpResponseStr
		 * = "";
		 * 
		 * GsonBuilder builder = new GsonBuilder();
		 * 
		 * // Register an adapter to manage the date types as long values
		 * builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>()
		 * {
		 * 
		 * @Override public Date deserialize(JsonElement json, Type typeOfT,
		 * JsonDeserializationContext context) throws JsonParseException {
		 * return new Date(json.getAsJsonPrimitive().getAsLong()); } });
		 * 
		 * // make a serializer for date the other way
		 * builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
		 * 
		 * @Override public JsonElement serialize(Date src, Type typeOfSrc,
		 * JsonSerializationContext context) { return src == null ? null : new
		 * JsonPrimitive(src.getTime()); } });
		 * 
		 * gson = builder.create();
		 * 
		 * tasksToDo = new Stack<Cashflow>(); for (Cashflow c : cashflows) if
		 * (!c.isUploaded()) { UserDetails userDetails = new UserDetails();
		 * userDetails.setUsername(myApp.getUsername());
		 * userDetails.setPassword(myApp.getPassword());
		 * c.setUserDetails(userDetails); tasksToDo.add(c); }
		 * 
		 * if (tasksToDo.size() > 0) { currentCashflow = tasksToDo.pop();
		 * pb.setVisibility(View.VISIBLE); Log.d(BaseActivity.TAG,
		 * gson.toJson(currentCashflow)); new
		 * MyAsyncTask().execute(gson.toJson(currentCashflow)); } else
		 * Toast.makeText(this, "Everything is up to date",
		 * Toast.LENGTH_SHORT).show(); }
		 */
	}

	public void openSettings(View v) {
		AuthDialog authDialog = new AuthDialog();
		authDialog.show(getFragmentManager(), "Authentication");
	}

	static class AuthDialog extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View view = inflater.inflate(R.layout.dialog_authentication, null);
			final EditText username = (EditText) view.findViewById(R.id.etUsername);
			final EditText password = (EditText) view.findViewById(R.id.etPassword);
			builder.setView(view).setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
					Editor editor = prefs.edit();
					Gson gson = new Gson();
					UserDetails userDetails = new UserDetails();
					userDetails.setUsername(username.getText().toString().trim());
					userDetails.setPassword(Security.md5(password.getText().toString()));
					// userDetails.setPassword(password.getText().toString());
					String json = gson.toJson(userDetails);
					editor.putString("userdetails", json);
					editor.commit();
				}
			}).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					AuthDialog.this.getDialog().cancel();
				}
			});
			return builder.create();
		}
	}

	private class RestAPIConnector extends AsyncTask<String, Void, Void> {
		private ProgressDialog Dialog = new ProgressDialog(MainActivity.this);
		HttpResponse response;

		@Override
		protected void onPreExecute() {
			// NOTE: You can call UI Element here.

			// Start Progress Dialog (Message)

			Dialog.setMessage("Uploading to Server. Please wait..");
			Dialog.show();
		}

		@Override
		protected Void doInBackground(String... urls) {
			// cashflows;
			try {
				GsonBuilder builder = new GsonBuilder();

				// Register an adapter to manage the date types as long values
				builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

					@Override
					public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
						return new Date(json.getAsJsonPrimitive().getAsLong());
					}
				});

				// make a serializer for date the other way
				builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
					@Override
					public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
						return src == null ? null : new JsonPrimitive(src.getTime());
					}
				});

				gson = builder.create();

				ArrayList<Cashflow> synchList = new ArrayList<Cashflow>();
				for (Cashflow loan : cashflows)
					if (!loan.isUploaded())
						synchList.add(loan);

				// Gson gson = new Gson();
				String json = gson.toJson(synchList);
				String username = myApp.getUsername();
				HttpPost httpPost = new HttpPost(Endpoint.CASHFLOW_ADD + "?username=" + myApp.getUsername() + "&password="
						+ myApp.getPassword());
				// HttpPost httpPost = new
				// HttpPost("http://10.12.124.55:8080/rest/cashflows?username=humaid&password=pass");
				httpPost.setEntity((HttpEntity) new StringEntity(json));
				// httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				// set timeout
				HttpParams httpParameters = new BasicHttpParams();
				int timeoutConnection = TIMEOUT;
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				// end of set timeout
				/*
				 * httpParameters.setParameter("username", myApp.getUsername());
				 * httpParameters.setParameter("password", myApp.getPassword());
				 */
				DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

				response = httpClient.execute(httpPost);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void unused) {
			// Close progress dialog
			Dialog.dismiss();
			if (response == null) {
				Toast.makeText(MainActivity.this, "Unable to connect to Server. Plz enable Internet", Toast.LENGTH_SHORT).show();
				return;
			}
			if (response.getStatusLine().getStatusCode() == 200) {
				/*
				 * deleteCashflows(); cashflows.clear();
				 * //adapter.notifyDataSetChanged(); adapter.clear();
				 */
				for (Cashflow loan : cashflows)
					loan.setUploaded(true);
				saveCashflows();
				adapter.notifyDataSetChanged();
				Toast.makeText(MainActivity.this, "Synch Complete", Toast.LENGTH_SHORT).show();
				return;
			}
			if (response.getStatusLine().getStatusCode() == 401)
				Toast.makeText(MainActivity.this, "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(MainActivity.this, "Synch Failed", Toast.LENGTH_SHORT).show();
		}
	}

	// Kyle's Code
	/*
	 * private class MyAsyncTask extends AsyncTask<String, Integer, Double> {
	 * 
	 * @Override protected Double doInBackground(String... params) { // TODO
	 * Auto-generated method stub postData(params[0]); return null; }
	 * 
	 * @Override protected void onPostExecute(Double result) { refreshNow(); }
	 * 
	 * @Override protected void onProgressUpdate(Integer... progress) {
	 * pb.setProgress(progress[0]); }
	 * 
	 * public void postData(String json) { // Create a new HttpClient and Post
	 * Header try { HttpPost post = new HttpPost();
	 * 
	 * String uri = Endpoint.CASHFLOW_ADD;
	 * 
	 * post.setURI(new URI(uri));
	 * 
	 * post.setHeader("Content-Type", "application/json; charset=utf-8");
	 * post.setEntity(new StringEntity(json, HTTP.UTF_8));
	 * 
	 * // Log.d(BaseActivity.TAG, "URI: " + uri + " JSON: " + json);
	 * 
	 * // set timeout HttpParams httpParameters = new BasicHttpParams();
	 * 
	 * int timeoutConnection = TIMEOUT;
	 * HttpConnectionParams.setConnectionTimeout(httpParameters,
	 * timeoutConnection); // end of set timeout
	 * 
	 * DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
	 * HttpResponse httpResponse; httpResponse = httpClient.execute(post);
	 * 
	 * if (httpResponse.getStatusLine().getStatusCode() == 200) ok = true; //
	 * Log.d(BaseActivity.TAG, "STATUS_CODE: " + //
	 * httpResponse.getStatusLine().getStatusCode()); httpResponseStr =
	 * Caller.convertInputStreamToString(httpResponse.getEntity().getContent());
	 * 
	 * } catch (Exception e) { e.printStackTrace(); httpResponseStr = ERROR; } }
	 * 
	 * }
	 */
}
