package com.spbd.cashflow.app.model;

import java.io.Serializable;

public class Diary implements Serializable {

	private String id;

	private int monthNumber;

	private double businessIn;

	private double businessOut;

	private double householdIn;

	private double householdOut;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getMonthNumber() {
		return monthNumber;
	}

	public void setMonthNumber(int monthNumber) {
		this.monthNumber = monthNumber;
	}

	public double getBusinessIn() {
		return businessIn;
	}

	public void setBusinessIn(double businessIn) {
		this.businessIn = businessIn;
	}

	public double getBusinessOut() {
		return businessOut;
	}

	public void setBusinessOut(double businessOut) {
		this.businessOut = businessOut;
	}

	public double getHouseholdIn() {
		return householdIn;
	}

	public void setHouseholdIn(double householdIn) {
		this.householdIn = householdIn;
	}

	public double getHouseholdOut() {
		return householdOut;
	}

	public void setHouseholdOut(double householdOut) {
		this.householdOut = householdOut;
	}

	@Override
	public String toString() {
		return "Diary: Month nr: " + getMonthNumber();
	}
}
