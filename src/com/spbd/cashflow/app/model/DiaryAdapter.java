package com.spbd.cashflow.app.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.spbd.cashflow.app.R;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class DiaryAdapter extends ArrayAdapter<Diary> {

	private ArrayList<Diary> diaries;
	
	/**
	 * Constructor
	 * @param context
	 * @param diaries
	 */
    public DiaryAdapter(Context context, ArrayList<Diary> diaries) {
       super(context, 0, diaries);
       this.diaries = new ArrayList<Diary>();
       this.diaries = diaries;
       
    }
    


    /**
     * getView function
     */
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	   Diary diary = diaries.get(position);
    	    
    	   if (convertView == null) {
    		convertView = LayoutInflater.from(getContext()).inflate(R.layout.diary_list_item, parent, false);
    		//set EditTexts
    	    EditText etBusIn = (EditText) convertView.findViewById(R.id.etBusIn);
    	    EditText etBusOut = (EditText) convertView.findViewById(R.id.etBusOut);
    	    EditText etHouseIn = (EditText) convertView.findViewById(R.id.etHouseIn);
    	    EditText etHouseOut = (EditText) convertView.findViewById(R.id.etHouseOut);
    	    //attach the TextWatcher listener to the EditText
    	    etBusIn.addTextChangedListener(new MyTextWatcher(convertView, etBusIn));
    	    etBusOut.addTextChangedListener(new MyTextWatcher(convertView, etBusOut));
    	    etHouseIn.addTextChangedListener(new MyTextWatcher(convertView, etHouseIn));
    	    etHouseOut.addTextChangedListener(new MyTextWatcher(convertView, etHouseOut));
    	   }
    	   
    	   //set EditTexts
    	   EditText etBusIn = (EditText) convertView.findViewById(R.id.etBusIn);
    	   EditText etBusOut = (EditText) convertView.findViewById(R.id.etBusOut);
   	       EditText etHouseIn = (EditText) convertView.findViewById(R.id.etHouseIn);
   	       EditText etHouseOut = (EditText) convertView.findViewById(R.id.etHouseOut);
    	   
   	       //set texts of EditTexts
    	   etBusIn.setTag(diary);
    	   if(diary.getBusinessIn() != 0){
    	    etBusIn.setText(String.valueOf(diary.getBusinessIn()));
    	   }
    	   else {
    	    etBusIn.setText("0");
    	   }
    	   etBusOut.setTag(diary);
    	   if(diary.getBusinessOut() != 0){
    	    etBusOut.setText(String.valueOf(diary.getBusinessOut()));
    	   }
    	   else {
    	    etBusOut.setText("0");
    	   }
    	   etHouseIn.setTag(diary);
    	   if(diary.getHouseholdIn() != 0){
    	    etHouseIn.setText(String.valueOf(diary.getHouseholdIn()));
    	   }
    	   else {
    	    etHouseIn.setText("0");
    	   }
    	   etHouseOut.setTag(diary);
    	   if(diary.getHouseholdOut() != 0){
    	    etHouseOut.setText(String.valueOf(diary.getHouseholdOut()));
    	   }
    	   else {
    	    etHouseOut.setText("0");
    	   }
    	    
    	   //set the month number in textview
    	   TextView tvMonthNumber = (TextView) convertView.findViewById(R.id.tvMonthNumber);
    	   tvMonthNumber.setText("Week: " + diary.getMonthNumber());
    	   
    	 
    	   return convertView;
    	 
    	  }
    	 
    	 }
    	  

			/** 
			 * Custom TextWatcher for EditTexts
			 * @author Charlotte
			 *
			 */
    	 class MyTextWatcher implements TextWatcher{
    	 
    	  private View view;
    	  private EditText et;
    	  
    	  /**
    	   * Constructor
    	   * @param view
    	   * @param et
    	   */
    	  MyTextWatcher(View view, EditText et) {
    	   this.view = view;
    	   this.et = et;
    	  }
    	 
    	  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    	   //do nothing
    	  }
    	  
    	  public void onTextChanged(CharSequence s, int start, int before, int count) {
    		  //do nothing
       	  }
    	  
    	 
    	 //get value of EditText and add to Diary object
    	  public void afterTextChanged(Editable s) {
          	 int length = et.getText().length();
          	 int id = et.getId();
          	 
          	 //get value of EditText  
         	 String valueString = s.toString().trim();
         	 double value = valueString.equals("") ? 0:Double.valueOf(valueString);
          	 
          	 int position = Double.toString(value).indexOf(".");
             int sel = et.getSelectionStart();
             
             if(value > 0.0 & sel <= position){
          	   et.setSelection(position);
             }else if (sel > position){
          	   et.setSelection(Double.toString(value).length()-1);
             }
             
             Diary diary;
         	   if(id == R.id.etBusIn) {
         		   EditText etBusIn = (EditText) view.findViewById(R.id.etBusIn);
         		   diary = (Diary) etBusIn.getTag();
         		   if(diary.getBusinessIn() != value){
             		   diary.setBusinessIn(value);
             		   if(diary.getBusinessIn() != 0){
             			   etBusIn.setText(String.valueOf(diary.getBusinessIn()));
             		   }
             		   else {
             			   etBusIn.setText("");
             		   }
             	    }
         	   } else if (id == R.id.etBusOut) {
         		   EditText etBusOut = (EditText) view.findViewById(R.id.etBusOut);
         		   diary = (Diary) etBusOut.getTag();
         		   if(diary.getBusinessOut() != value){
             		   diary.setBusinessOut(value);
             		   if(diary.getBusinessOut() != 0){
             			   etBusOut.setText(String.valueOf(diary.getBusinessOut()));
             		   }
             		   else {
             			   etBusOut.setText("");
             		   }
             	    }
         	   } else if (id == R.id.etHouseIn) {
         		   EditText etHouseIn = (EditText) view.findViewById(R.id.etHouseIn);
         		   diary = (Diary) etHouseIn.getTag();
         		   if(diary.getHouseholdIn() != value){
             		   diary.setHouseholdIn(value);
             		   if(diary.getHouseholdIn() != 0){
             			   etHouseIn.setText(String.valueOf(diary.getHouseholdIn()));
             		   }
             		   else {
             			   etHouseIn.setText("");
             		   }
             	    }
         	   } else if (id == R.id.etHouseOut) {
         		   EditText etHouseOut = (EditText) view.findViewById(R.id.etHouseOut);
         		   diary = (Diary) etHouseOut.getTag();
         		   if(diary.getHouseholdOut() != value){
             		   diary.setHouseholdOut(value);
             		   if(diary.getHouseholdOut() != 0){
             			   etHouseOut.setText(String.valueOf(diary.getHouseholdOut()));
             		   }
             		   else {
             			   etHouseOut.setText("");
             		   }
             	    }
         	   }
         	   return;
             
    	 }
  }
