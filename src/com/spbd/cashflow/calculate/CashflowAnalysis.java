package com.spbd.cashflow.calculate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.spbd.cashflow.app.model.Cashflow;
import com.spbd.cashflow.app.model.Diary;



public class CashflowAnalysis implements Serializable {
	private static final long serialVersionUID = 1L;

	private Cashflow cashflow;

	public CashflowAnalysis() {
		cashflow = new Cashflow();
		//For test puposes only
//		generateTestData();
	}
	
	public CashflowAnalysis(Cashflow cashflow) {
		this.cashflow = cashflow;
		//For test puposes only
//		generateTestData();
	}
	
	// TODO: Addd Methods for calculations

	// Diary calculations
	public void diaryCalculations() {
		//setNumberofDiaries
		cashflow.setNumberOfDiaryMonths(cashflow.getDiaries().size());
		//setDiaryBusinessInAverage
		double sum = 0;
		for (int i = 0; i < cashflow.getDiaries().size(); i++) {
			sum = sum + cashflow.getDiaries().get(i).getBusinessIn();
		}
		cashflow.setDiaryBusinessInAverage(truncate(sum / cashflow.getDiaries().size()));
		//setDiaryBusinessOutAverage
		sum = 0;
		for (int i = 0; i < cashflow.getDiaries().size(); i++) {
			sum = sum + cashflow.getDiaries().get(i).getBusinessOut();
		}
		cashflow.setDiaryBusinessOutAverage(truncate(sum / cashflow.getDiaries().size()));
		//setBusinessNetAverage
		cashflow.setDiaryBusinessNetAverage(truncate(cashflow
				.getDiaryBusinessInAverage()
				- cashflow.getDiaryBusinessOutAverage()));
		//setDiaryHouseholdInAverage
		sum = 0;
		for (int i = 0; i < cashflow.getDiaries().size(); i++) {
			sum = sum + cashflow.getDiaries().get(i).getHouseholdIn();
		}
		cashflow.setDiaryHouseholdInAverage(truncate(sum / cashflow.getDiaries().size()));
		//setDiaryHouseholdOutAverage
		sum = 0;
		for (int i = 0; i < cashflow.getDiaries().size(); i++) {
			sum = sum + cashflow.getDiaries().get(i).getHouseholdOut();
		}
		cashflow.setDiaryHouseholdOutAverage(truncate(sum / cashflow.getDiaries().size()));
		//setHouseholdNetAverage
		cashflow.setDiaryHouseholdNetAverage(truncate(cashflow
				.getDiaryHouseholdInAverage()
				- cashflow.getDiaryHouseholdOutAverage()));

	}

	private void generateTestData() {

		//Cashflow loan = new Cashflow();
		cashflow.setNumberOfDiaryMonths(1);
		cashflow.setDiaryBusinessInAverage(250);
		cashflow.setDiaryBusinessOutAverage(50);
		cashflow.setDiaryBusinessNetAverage(200);
		cashflow.setDiaryHouseholdInAverage(54);
		cashflow.setDiaryHouseholdOutAverage(100);
		cashflow.setDiaryHouseholdNetAverage(46);
		cashflow.setExpectedBusinessInAverage(300);
		cashflow.setExpectedBusinessOutAverage(70);
		cashflow.setExpectedBusinessNetAverage(30);
		cashflow.setExpectedHouseholdInAverage(90);
		cashflow.setExpectedHouseholdOutAverage(65);
		cashflow.setExpectedHouseholdNetAverage(20);

		cashflow.setLoanAmount(1000.0);
		cashflow.setAccepted(true);
		cashflow.setSummary("High Risk loan");
	//	loan.setDifferenceBetweenPresentAndExpectedHouseholdRisk("high");
	/*	Applicant cust = new Applicant();
		cust.setId("8abe16fd4883f3d0014883f4c3600001");*/
		List<Diary> diaryList = new ArrayList<Diary>();
		Diary diary = new Diary();
		diary.setBusinessIn(10.0);
		diary.setBusinessOut(10.0);
		diary.setHouseholdIn(92.0);
		diary.setHouseholdOut(992.0);
		diary.setMonthNumber(1);
		diaryList.add(diary);
		cashflow.setDiaries(diaryList);
	//	cashflow.setApplicant(cust);

	}

	// OUTPUT PAGE CALCULATIONS
	public void calculateOutput() {

		// Cal 1
		if(cashflow.getDiaryBusinessInAverage()!=0.0)
		{
			cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiary(truncate(100*cashflow.getDiaryBusinessNetAverage()/cashflow.getDiaryBusinessInAverage()));
			cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(Criteria.calculateRisk(0, cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiary()));
		}
		else
		{
			cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiary(0.0);
			cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(Criteria.NARISK);
		}
		
		// Cal 2
		if(cashflow.getExpectedBusinessInAverage()!=0.0)
		{
			cashflow.setNetBusinessIncomeAsSalesRevenueBudget(truncate(100
				* cashflow.getExpectedBusinessNetAverage()
				/ cashflow.getExpectedBusinessInAverage()));
		cashflow.setNetBusinessIncomeAsSalesRevenueBudgetRisk(Criteria.calculateRisk(1, cashflow.getNetBusinessIncomeAsSalesRevenueBudget()));
		}
		else
		{
			cashflow.setNetBusinessIncomeAsSalesRevenueBudget(0.0);
			cashflow.setNetBusinessIncomeAsSalesRevenueBudgetRisk(Criteria.NARISK);
		}
		// Cal 3
		if(cashflow.getDiaryBusinessInAverage()!=0.0)
		{
		cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusiness(truncate((cashflow.getExpectedBusinessInAverage()- cashflow.getDiaryBusinessInAverage())/cashflow.getDiaryBusinessInAverage()));
		cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(Criteria.calculateRisk(2, cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusiness()));
		}
		else
		{
			cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusiness(0.0);
			cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(Criteria.NARISK);

		}
		//HARDCODED WEEKLY LOAN REPAYMENT(NEED TO CHANGE LATER)
		//double weeklyLoanRepayment=cashflow.getLoanAmount()/12;
		cashflow.setWeeksToPay(Criteria.calculateWeekstoPay(cashflow.getLoanAmount()));
		cashflow.setWeeklyRepayment(truncate(cashflow.getLoanAmount()/cashflow.getWeeksToPay()));
		
		
		// Cal 4
		if(cashflow.getDiaryHouseholdInAverage()!=0)
		{
		cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiary(truncate(100
				* cashflow.getWeeklyRepayment()
				/ cashflow.getDiaryHouseholdInAverage()));
		cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(Criteria.calculateRisk(3, cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiary()));
		}
		else
		{
			cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiary(0.0);
			cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(Criteria.NARISK);
			
		}
		// Cal 5
		if(cashflow.getExpectedHouseholdInAverage()!=0.0)
		{
		cashflow.setLoanRepaymentOfHouseholdIncomeBudget(truncate(100
				* cashflow.getWeeklyRepayment()
				/ cashflow.getExpectedHouseholdInAverage()));
		cashflow.setLoanRepaymentOfHouseholdIncomeBudgetRisk(Criteria.calculateRisk(4, cashflow.getLoanRepaymentOfHouseholdIncomeBudget()));
		}
		else
		{
			cashflow.setLoanRepaymentOfHouseholdIncomeBudget(0.0);
			cashflow.setLoanRepaymentOfHouseholdIncomeBudgetRisk(Criteria.NARISK);
		}

		if(cashflow.getDiaryHouseholdInAverage()!=0.0)
		{
			cashflow.setDifferenceBetweenPresentAndExpectedHousehold(truncate((cashflow.getExpectedHouseholdInAverage()- cashflow.getDiaryHouseholdInAverage())/cashflow.getDiaryHouseholdInAverage()));
			cashflow.setDifferenceBetweenPresentAndExpectedHouseholdRisk(Criteria.calculateRisk(5, cashflow.getDifferenceBetweenPresentAndExpectedHousehold()));
		}
		else
		{
			cashflow.setDifferenceBetweenPresentAndExpectedHousehold(0.0);
			cashflow.setDifferenceBetweenPresentAndExpectedHouseholdRisk(Criteria.NARISK);

		}

		
		// Cal 7
		if(cashflow.getDiaryHouseholdInAverage()!=0)
		{
		cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiary(truncate(100
				* cashflow.getDiaryHouseholdOutAverage()
				/ cashflow.getDiaryHouseholdInAverage()));
		cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(Criteria.calculateRisk(6, cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiary()));
		}
		else
		{
			cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiary(0.0);
			cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(Criteria.NARISK);
		}
		
		// Cal 8
		if(cashflow.getDiaryHouseholdInAverage()!=0.0)
		{
		cashflow.setHouseholdExpensesOfHouseholdIncomeBudget((truncate(100* cashflow.getDiaryHouseholdOutAverage()/ cashflow.getDiaryHouseholdInAverage())));
		cashflow.setHouseholdExpensesOfHouseholdIncomeBudgetRisk(Criteria.calculateRisk(7, cashflow.getHouseholdExpensesOfHouseholdIncomeBudget()));
		
		}else{
			cashflow.setHouseholdExpensesOfHouseholdIncomeBudget(0.0);
			cashflow.setHouseholdExpensesOfHouseholdIncomeBudgetRisk(Criteria.NARISK);
		}	
		
		// CALCULATE SUMMARY ///////////////////////////////////////
		int totalRed=0;
		int totalYellow=0;
		int totalGreen=0;
		int totalNA=0;
		// cashflow analysis 1
		
		if(cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk().equals(Criteria.NARISK))
			totalNA++;

		// cashflow analysis 2		
		if(cashflow.getNetBusinessIncomeAsSalesRevenueBudgetRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueBudgetRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueBudgetRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getNetBusinessIncomeAsSalesRevenueBudgetRisk().equals(Criteria.NARISK))
			totalNA++;

		// cashflow analysis 3
		if(cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk().equals(Criteria.NARISK))
			totalNA++;


		// cashflow analysis 4

		if(cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk().equals(Criteria.NARISK))
			totalNA++;

		// cashflow analysis 5

		if(cashflow.getLoanRepaymentOfHouseholdIncomeBudgetRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeBudgetRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeBudgetRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getLoanRepaymentOfHouseholdIncomeBudgetRisk().equals(Criteria.NARISK))
			totalNA++;
		

		// cashflow analysis 6
		if(cashflow.getDifferenceBetweenPresentAndExpectedHouseholdRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedHouseholdRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedHouseholdRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getDifferenceBetweenPresentAndExpectedHouseholdRisk().equals(Criteria.NARISK))
			totalNA++;
		
		// cashflow analysis 7
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk().equals(Criteria.NARISK))
			totalNA++;

		// cashflow analysis 8
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeBudgetRisk().equals(Criteria.HIGHRISK))
			totalRed++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeBudgetRisk().equals(Criteria.MEDRISK))
			totalYellow++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeBudgetRisk().equals(Criteria.LOWRISK))
			totalGreen++;
		if(cashflow.getHouseholdExpensesOfHouseholdIncomeBudgetRisk().equals(Criteria.NARISK))
			totalNA++;
		
		
		if(cashflow.getNumberOfDiaryMonths()>0)
		{
			if(totalRed>3 || totalNA>=1)
			{
				cashflow.setSummary(Criteria.SUMMARY[4]);
				return;
			}
			if((totalRed>=2 && totalRed<=3) || (totalRed==1 && totalYellow>3))
			{
				cashflow.setSummary(Criteria.SUMMARY[3]);
				return;
			}
			if(totalYellow>4 || (totalRed==1 && totalYellow<3))
			{
				cashflow.setSummary(Criteria.SUMMARY[2]);
				return;
			}
			if(totalYellow>=2 && totalYellow<=4)
			{
				cashflow.setSummary(Criteria.SUMMARY[1]);
				return;
			}
			cashflow.setSummary(Criteria.SUMMARY[0]);
			return;
		}
		else
		{
			if(totalRed>2 || totalNA>=1)
			{
				cashflow.setSummary(Criteria.SUMMARY[4]);
				return;
			}
			if((totalRed>=1 && totalRed<=2))
			{
				cashflow.setSummary(Criteria.SUMMARY[3]);
				return;
			}
			if(totalYellow>2)
			{
				cashflow.setSummary(Criteria.SUMMARY[2]);
				return;
			}
			if(totalYellow==2)
			{
				cashflow.setSummary(Criteria.SUMMARY[1]);
				return;
			}
			cashflow.setSummary(Criteria.SUMMARY[0]);
			return;
		}
		
	}

	// Set double precision
	private static double truncate(double number) {
		BigDecimal bd = new BigDecimal(number);
		bd = bd.setScale(2, RoundingMode.FLOOR);
		return bd.doubleValue();
	}

	// GETTERS & SETTERS
	public Cashflow getCashflow() {
		return cashflow;
	}

	public void setCashflow(Cashflow cashflow) {
		this.cashflow = cashflow;
	}

}
